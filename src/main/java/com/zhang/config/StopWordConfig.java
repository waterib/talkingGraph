package com.zhang.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class StopWordConfig {
    @Bean
    @Scope("singleton")
    public List<String> stopWords() throws IOException{

        List<String> list = new ArrayList<>();

        File file = new File("src/main/resources/baidu_stopwords.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);

        String line = null;

        while ((line = reader.readLine()) != null){
            list.add(line);
        }

        return list;
    }
}
