package com.zhang.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhang.dao.UserCount;
import com.zhang.dao.WordCount;
import com.zhang.service.redis.RedisServiceInter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WordController {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    @Qualifier("redisServiceImpl")
    RedisServiceInter redisServiceImpl;

    @GetMapping("/getWBC/{count}")
    public String getWordByCount(@PathVariable int count) throws JsonProcessingException {
        List<WordCount> wordByCount = redisServiceImpl.getWordByCount(count);
        String res = objectMapper.writeValueAsString(wordByCount);
        return res;
    }

    @GetMapping("/getWBU/{count}")
    public String getWordByUser(@PathVariable int count) throws JsonProcessingException {
        List<UserCount> userByCount = redisServiceImpl.getUserByCount(count);
        String res = objectMapper.writeValueAsString(userByCount);
        return res;
    }
}
