package com.zhang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SocketController {
    @RequestMapping("/toIndex")
    public String toIndex(String nickname, Model model){
        model.addAttribute("nickname", nickname);
        return "index";
    }

    @RequestMapping("/")
    public String toLogin(){
        return "login";
    }

    @RequestMapping("/wc")
    public String toWordCount(){
        return "WordCount";
    }
}
