package com.zhang.service.message;

interface MessageServiceInter {
    void sendUserMassageAndNameToAll(String msg);
    void sendUserMessageToAll(String msg);
    void sendUserNameToAll(String name);
}
