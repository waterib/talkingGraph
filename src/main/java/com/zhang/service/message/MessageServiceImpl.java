package com.zhang.service.message;

import com.zhang.service.kafka.KafkaServiceInter;
import com.zhang.service.redis.RedisServiceInter;
import com.zhang.utils.IKSegmenterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageServiceInter {

    @Autowired
    @Qualifier("kafkaServiceAlpha")
    KafkaServiceInter kafkaServiceAlpha;

    @Autowired
    RedisServiceInter redisServiceImpl;

    @Autowired
    IKSegmenterUtils ikSegmenterUtils;

    @Value("${graph.open}")
    boolean sendIsOpen;

    @Override
    public void sendUserMassageAndNameToAll(String msg) {
        String[] split = msg.split("ꅄ");
        sendUserNameToAll(split[1]);
        sendUserMessageToAll(split[2]);
    }

    @Override
    public void sendUserMessageToAll(String msg) {
        try {
            List<String> stringList = ikSegmenterUtils.getStringList(msg);
            for (String s : stringList) {
                redisServiceImpl.sendUserMassage(s);
                kafkaServiceAlpha.sendUserMessage(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendUserNameToAll(String name) {
        redisServiceImpl.sendUserName(name);
    }
}
