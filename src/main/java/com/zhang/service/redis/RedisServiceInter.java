package com.zhang.service.redis;

import com.zhang.dao.UserCount;
import com.zhang.dao.WordCount;

import java.util.List;

public interface RedisServiceInter {
    // 发送消息
    void sendUserMassage(String msg);
    // 发送用户名字
    void sendUserName(String name);

    // 获取消息及词频
    List<WordCount> getWordByCount(int count);

    // 获取用户消息发送
    List<UserCount> getUserByCount(int count);
}
