package com.zhang.service.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaServiceAlpha implements KafkaServiceInter {

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.use.topic}")
    private String topic; // topic名称

    @Override
    public void sendUserMessage(String msg) {
        kafkaTemplate.send(topic, msg);
    }
}
