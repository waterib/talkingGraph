package com.zhang.service.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class KafkaService implements KafkaServiceInter {

    @Autowired
    private KafkaProducer<String, String> kafkaProducer;

    @Value("${kafka.use.topic}")
    private String topic; // topic名称

    public void sendUserMessage(String msg){
        ProducerRecord<String,String> producerRecord = new ProducerRecord<>(topic, "taking", msg);

        try {
            kafkaProducer.send(producerRecord, (success, error) -> {
                System.out.println("success：" + success.toString());
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
