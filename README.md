### 一个聊天室

###### 一个基于Springboot的websocket聊天室+词频可视化



![](img/1.png)

![](img/2.png)

#### 部署方式

修改resource目录下`templates/index.html`中script标签下连接wc协议的IP服务器地址

修改application.properties中redis地址（后台数据库基于redis）

内嵌了发送消息到kafka，不使用直接关闭即可，MessageService中可以选择sink

`mvn install package`

没别的了